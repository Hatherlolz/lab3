﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        public delegate double FuncDelegate(double x);
        public static double Function(double x)
        {
            double sin = Math.Pow(Math.Sin(2 * x), 3);
            return sin + 45 + 5 * x / 2 * Math.Pow(2 * x, 4) + 4 * 5 * x;
        }

        public static double Integral(double a, double b, double n, FuncDelegate func)
        {

            double dx = (b - a / n);
            double y1, y2;
            double x1, x2;
            double Intgrl = 0;
            double Intgrll = 0;
            double Intgrlr = 0;

            for (int i = 0; i < n; i++)
            {
                x1 = a + i * dx;
                x2 = x1 + dx;
                y1 = func(x1);
                y2 = func(x2);

                // Обчислення інтегралу методом центральних прямокутників
                Intgrl += (y1 + y2) / 2 * dx;
                // Обчислення інтегралу методом центральних прямокутників
                Intgrll += y1 * (y2 - y1);
                // Обчислення інтегралу методом центральних прямокутників
                Intgrlr += y2 * (y2 - y1);
            }
            return Intgrl;
        }

        /* public static double Integrall(double a, double b, double n, FuncDelegate func)
         {

             double dx = (b - a / n);
             double y1, y2;
             double x1, x2;
             double Intgrll = 0;

             for (int i = 0; i < n; i++)
             {
                 x1 = a + i * dx;
                 x2 = x1 + dx;
                 y1 = func(x1);
                 y2 = func(x2);

                 // Обчислення інтегралу методом центральних прямокутників
                 Intgrll += y1 * (y2 - y1);
             }
             return Intgrll;
         }*/

        /* public static double Integralr(double a, double b, double n, FuncDelegate func)
        {

            double dx = (b - a / n);
            double y1, y2;
            double x1, x2;
            double Intgrlr = 0;

            for (int i = 0; i < n; i++)
            {
                x1 = a + i * dx;
                x2 = x1 + dx;
                y1 = func(x1);
                y2 = func(x2);

                // Обчислення інтегралу методом центральних прямокутників
                Intgrlr += y2 * (y2 - y1);
            }
            return Intgrlr;
        } */

        static void Main(string[] args)
        {
        StartOfCalculations:
            Console.Write("Введiть початок вiдрiзку iнтегрування a: ");
            string sa = Console.ReadLine();
            double a = double.Parse(sa);

            Console.Write("Введiть початок вiдрiзку iнтегрування b: ");
            string sb = Console.ReadLine();
            double b = double.Parse(sb);

            Console.Write("Введiть кількість ділянок n: ");
            string sn = Console.ReadLine();
            double n = double.Parse(sn);

            double Intgrl;
            double Intgrll;
            double Intgrlr;

            FuncDelegate func = Function;

            Intgrl = Integral(a, b, n, func);
           /* Intgrll = Integrall(a, b, n, func);
            Intgrlr = Integralr(a, b, n, func);*/


            Console.WriteLine("Iнтеграл функцiї на вiдрiзку [{0}, {1}] = {2:0.0000}", a, b, Intgrl);
          /*  Console.WriteLine("Iнтеграл функцiї на вiдрiзку [{0}, {1}] = {2:0.0000}", a, b, Intgrll);
            Console.WriteLine("Iнтеграл функцiї на вiдрiзку [{0}, {1}] = {2:0.0000}", a, b, Intgrlr);*/

            Console.Write("Повторити розрахунок (y - так) ?: ");
            ConsoleKeyInfo pressedKey = Console.ReadKey();
            Console.WriteLine();
            if (pressedKey.Key == ConsoleKey.Y)
            {
                Console.WriteLine();
                goto StartOfCalculations;
            }

        }
    }
}
